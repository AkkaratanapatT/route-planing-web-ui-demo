import MainPage from "./pages/MainPage/MainPage";
import IPage from './interfaces/pages';

const routes: IPage[] = [
  {
    path: '/',
    name: 'Main Page',
    component: MainPage,
    exact: true,
  },
];

export {
  routes
};