import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Switch, Route, RouteComponentProps } from 'react-router-dom';
import { routes } from './routes';

const App: React.FunctionComponent = ({ }) => {
  return (
    <Router>
      <Switch>
        {
          routes.map((each, key) => {
            return (
              <Route
                key={key}
                path={each.path}
                exact={each.exact}
                render={(props: RouteComponentProps<any>) => (
                  <each.component
                    name={each.name}
                    {...props}
                    {...each.props}
                  />
                )}
              />
            )
          })
        }
      </Switch>
    </Router>
  );
};

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.tsx</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
