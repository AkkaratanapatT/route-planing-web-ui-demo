import React, { useState } from "react";
import {
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  Button,
  Drawer,
  Icon,
} from "@mui/material";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

interface IMenuProps {
  customStyle?: {};
  onSubmit?: () => Promise<void>;
  onOccupy?: () => Promise<void>;
  onUnoccupy?: () => Promise<void>;
  onLoadInProgress?: () => Promise<void>;
  onReset?: (event: any) => Promise<void>;
  selectedStartDate?: Date;
  selectedEndDate?: Date;
  onHandleStartDate?: (event: any) => void;
  onHandleEndDate?: (event: any) => void;
  layer?: number;
  onHandleLayer?: (event: any) => void;
  jobId?: string;
  onHandlerJobId?: (event: any) => void;
  onHandleBaseHeight: (event: any) => void;
  customHeight: number;
  heightVolume?: number;
  onHandlerHeight?: (event: any) => void;
  onSetVolume?: (event: any) => Promise<void>;
}

const Menu: React.FunctionComponent<IMenuProps> = ({
  customStyle,
  onSubmit,
  onOccupy,
  onUnoccupy,
  selectedEndDate,
  selectedStartDate,
  onHandleEndDate,
  onHandleStartDate,
  layer,
  onHandleLayer,
  jobId,
  onHandlerJobId,
  onLoadInProgress,
  onReset,
  customHeight,
  onHandleBaseHeight,
  heightVolume,
  onHandlerHeight,
  onSetVolume
}) => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  return (
    <>
      <div
        onClick={() => setIsDrawerOpen(true)}
        style={{
          position: "fixed",
          left: 15,
          top: 15,
          width: 30,
          height: 30,
          backgroundColor: "#fff",
          zIndex: 1,
          padding: "10px 15px",
          display: "flex",
          flexDirection: "column",
          borderRadius: 10,
          justifyContent: "center",
          alignItems: "center",
          ...customStyle,
        }}
      >
        <span>M</span>
        {/* <FontAwesomeIcon icon="house" /> */}
      </div>
      <Drawer open={isDrawerOpen} onClose={() => setIsDrawerOpen(false)}>
        <FormControl
          sx={{ m: 1, minWidth: 120 }}
          style={{ marginTop: 15 }}
          size="small"
        >
          <TextField
            label="Base height"
            value={customHeight}
            onInput={onHandleBaseHeight}
            defaultValue="0"
            size="small"
          />
        </FormControl>
        <FormControl
          sx={{ m: 1, minWidth: 120 }}
          style={{ marginTop: 15 }}
          size="small"
        >
          <InputLabel id="demo-select-small">Layer</InputLabel>
          <Select
            labelId="demo-select-small"
            id="demo-select-small"
            value={layer}
            label="Layer"
            onChange={onHandleLayer!}
          >
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={2}>2</MenuItem>
            <MenuItem value={3}>3</MenuItem>
            <MenuItem value={4}>4</MenuItem>
          </Select>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DateTimePicker
              renderInput={(props) => (
                <TextField style={{ marginTop: 10 }} size="small" {...props} />
              )}
              label="Start Time"
              value={selectedStartDate}
              onChange={onHandleStartDate!}
            />
          </LocalizationProvider>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DateTimePicker
              renderInput={(props) => (
                <TextField style={{ marginTop: 10 }} size="small" {...props} />
              )}
              label="End Time"
              value={selectedEndDate}
              onChange={onHandleEndDate!}
            />
          </LocalizationProvider>
        </FormControl>
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <Button onClick={onSubmit} variant="contained" size="small">
            Submit Start & End Point
          </Button>
        </FormControl>
        <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
          <TextField
            label="Job Id"
            value={jobId}
            onInput={onHandlerJobId!}
            defaultValue="Job Id"
            size="small"
          />
          <Button
            variant="contained"
            onClick={onOccupy}
            size="small"
            style={{ marginTop: 5 }}
          >
            Occupy Job
          </Button>
          <Button
            variant="contained"
            size="small"
            onClick={onUnoccupy}
            style={{ marginTop: 5 }}
          >
            Unoccupy Job
          </Button>
          <Button
            variant="contained"
            size="small"
            onClick={onLoadInProgress}
            style={{ marginTop: 5 }}
          >
            Load In Progress
          </Button>
          <Button
            variant="contained"
            size="small"
            onClick={onReset}
            style={{ marginTop: 5 }}
          >
            Reset
          </Button>
          <br></br>
          <TextField
            label="Height's volume"
            value={heightVolume}
            onInput={onHandlerHeight!}
            defaultValue="500"
            size="small"
          />

          <Button
            variant="contained"
            size="small"
            onClick={onSetVolume}
            style={{ marginTop: 5 }}
          >
            Set new height
          </Button>
        </FormControl>
      </Drawer>
    </>
  );
};

export default Menu;
