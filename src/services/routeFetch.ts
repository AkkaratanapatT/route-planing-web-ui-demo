import axios, { AxiosRequestConfig, AxiosInstance } from 'axios';

const axiosInstance = (): AxiosInstance => {
  const instance = axios.create({
    baseURL: 'http://docker.for.mac.localhost:3000',
    headers: {
      'accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      // 'Access-Control-Allow-Credentials': true,
    }
  })
  return instance;
}

export default {
  get: axiosInstance().get,
  put: axiosInstance().put,
  post: axiosInstance().post,
  delete: axiosInstance().delete,
}
// // Add a request interceptor
// axios.interceptors.request.use(
//     function (config:AxiosRequestConfig) {
//         // Do something before request is sent
//         //   config.headers.Authorization = `Bearer ${your_token}`;
//         // OR config.headers.common['Authorization'] = `Bearer ${your_token}`;
//         config.baseURL = 'https://example.io/api/';

//         return config;
//     },
//     function (error) {
//         // Do something with request error
//         return Promise.reject(error);
//     }
// );

// export default {
//     get: axios.get,
//     post: axios.post,
//     put: axios.put,
//     delete: axios.delete,
//     patch: axios.patch
// };