import React from "react";
import mapboxgl, { Marker } from "mapbox-gl";
import { useMap } from "./MainPageHelper";
import { CircularProgress } from "@mui/material";
import IPage from "../../interfaces/pages";
import "./MainPage.css";
import DatePicker from "react-datepicker";
import Menu from '../../components/Menu/Menu';
import "react-datepicker/dist/react-datepicker.css";

mapboxgl.accessToken =
  "pk.eyJ1IjoiYWx0ZWFyIiwiYSI6ImNsMDlmZXFwMjA5dXkzY3JxYnZndWJ4eWkifQ.9XiPDHd4pE1N4wLt2t9KTQ";

const MainPage: React.FunctionComponent<IPage> = ({ props }) => {
  const {
    lng,
    lat,
    zoom,
    mapContainer,
    loading,
    layer,
    clearPin,
    layerChange,
    submitToService,
    occupyPath,
    unoccupyPath,
    jobId,
    handleJobId,
    handleStartTime,
    handleCustomHeight,
    handleEndTime,
    startDateTime,
    endDateTime,
    loadInProgress,
    resetMap,
    customHeight,
    heightVolume,
    handleHeightVolume,
    setHightVolume
  } = useMap();

  return (
    <div className="container">
      <Menu
        onSubmit={submitToService}
        onOccupy={occupyPath}
        onUnoccupy={unoccupyPath}
        onHandleStartDate={handleStartTime}
        selectedStartDate={startDateTime}
        onHandleEndDate={handleEndTime}
        selectedEndDate={endDateTime}
        customHeight={customHeight}
        onHandleBaseHeight={handleCustomHeight}
        layer={layer}
        onHandleLayer={layerChange}
        jobId={jobId}
        onHandlerJobId={handleJobId}
        onLoadInProgress={loadInProgress}
        onReset={resetMap}
        heightVolume={heightVolume}
        onHandlerHeight={handleHeightVolume}
        onSetVolume={setHightVolume}

      />
      <div className={loading ? "dim" : "none"}>
        <div className="loading">
          <CircularProgress />
        </div>
      </div>
      <div>
        <div ref={mapContainer} className="map-container" />
      </div>
    </div>
  );
};

export default MainPage;
