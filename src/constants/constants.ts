export const layerHeight: {
  baseHeight: number;
  height: number;
  color: string;
}[] = [
  { baseHeight: 100, height: 180, color: "black" },
  { baseHeight: 200, height: 280, color: "red" },
  { baseHeight: 300, height: 380, color: "pink" },
  { baseHeight: 400, height: 480, color: "orange" },
  { baseHeight: 0, height: 500, color: "blue" },
];
